;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;; IDENTITY ;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
; (setq user-full-name "John Doe"
;       user-mail-address "john@doe.com")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;; FONTS ;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))
(setq doom-font (font-spec :family "Mononoki Nerd Font" :size 19 :weight 'bold) 
      doom-variable-pitch-font (font-spec :family "Mononoki Nerd Font" :size 19 :weight 'bold))

; (defvar required-fonts '("JetBrainsMono.*" "Overpass" "Madeupfont" "JuliaMono" "IBM Plex Mono" "Merriweather" "Alegreya"))

; (defvar available-fonts
;   (delete-dups (or (font-family-list)
;                    (split-string (shell-command-to-string "fc-list : family")
;                                  "[,\n]"))))

; (defvar missing-fonts
;   (delq nil (mapcar
;              (lambda (font)
;                (unless (delq nil (mapcar (lambda (f)
;                                            (string-match-p (format "^%s$" font) f))
;                                          available-fonts))
;                  font))
;              required-fonts)))
; (setq missing-fonts t)
; (if missing-fonts
;     (pp-to-string
;      `(unless noninteractive
;         (add-hook! 'doom-init-ui-hook
;           (run-at-time nil nil
;                        (lambda ()
;                          (message "%s missing the following fonts: %s"
;                                   (propertize "Warning!" 'face '(bold warning))
;                                   (mapconcat (lambda (font)
;                                                (propertize font 'face 'font-lock-variable-name-face))
;                                              ',missing-fonts
;                                              ", "))
;                          (sleep-for 5))))))
;   ";; No missing fonts detected")



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;; SOME DEFAULTS ;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; from here:
; https://tecosaur.github.io/emacs-config/config.html
(setq-default
 delete-by-moving-to-trash t                      ; Delete files to trash
 window-combination-resize t                      ; take new window space from all other windows (not just current)
 x-stretch-cursor t)                              ; Stretch cursor to the glyph width
(setq undo-limit 80000000                         ; Raise undo-limit to 80Mb
      evil-want-fine-undo t                       ; By default while in insert all changes are one big blob.
      ; auto-save-default t                       ; Nobody likes to loose work, I certainly don't
      truncate-string-ellipsis "…"                ; Unicode ellispis are nicer than "...", and also save /precious/ space
      ; password-cache-expiry nil                   ; I can trust my computers ... can't I?
      ;; scroll-preserve-screen-position 'always     ; Don't have `point' jump around
      scroll-margin 2)                            ; It's nice to maintain a little margin

(display-time-mode 1)                             ; Enable time in the mode-line


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;; LOOK & FEEL ;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Don't ask if themes are safe
(setq custom-safe-themes t)
; Show recent files
(recentf-mode) ;; Recent files
; Turn on tool-bar, menu-bar & scroll-bar
(tool-bar-mode 1)
(menu-bar-mode 1) ; if off can access menu with F10
(scroll-bar-mode 1)

; Start Window Maximized
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;Focus follows mouse:
(setq mouse-autoselect-window t)

; Sublime-Text style minimap
; (minimap-mode)

; Disable exit confirmation
(setq confirm-kill-emacs nil)

; Custom splash logo
(let ((alternatives '("doom-emacs-bw-light.svg"
                      "doom-emacs-flugo-slant_out_purple-small.png"
                      "doom-emacs-flugo-slant_out_bw-small.png"
                      ; "doom-emacs-bw-dark.svg"
                      "doom-emacs-color.png"
                      "doom-emacs-flugo-slant_out_bw.png"
                      "doom-emacs-slant-out-bw.png"
                      "doom-emacs-color2.png"
                      "doom-emacs-slant-out-color.png"
                      "doom-emacs-color2.svg"
                      "doom-emacs-flugo-slant_out_purple.png")))
  (setq fancy-splash-image
        (concat doom-private-dir "splash/"
                (nth (random (length alternatives)) alternatives))))


; Nano modeline
; https://github.com/rougier/nano-modeline
; (setq nano-modeline-position 'top)
; (setq nano-modeline-mode t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;; Functionality ;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Using emacs as a server (listens for external edit requests)
; https://www.gnu.org/software/emacs/manual/html_node/emacs/Emacs-Server.html
(server-start)

; Load pdf-tools
; https://github.com/politza/pdf-tools
; (pdf-tools-install)
(pdf-loader-install) ; faster alt to above



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;; Config Files ;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Load theme file:
(load "~/.doom.d/config/theme.el")

; Load TeX customizations file:
(load "~/.doom.d/config/tex.el")

; Load Prolog customizations file:
(load "~/.doom.d/config/prolog.el")

; Load org customizations file:
(load "~/.doom.d/config/org.el")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;; LINE NUMBERING ;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; If set to `nil', line numbers disabled. 
;; For relative line numbers, set to `relative'.
(global-display-line-numbers-mode)
(setq display-line-numbers-type 't)

(global-visual-line-mode) ;; Wrap lines

; Choose which major modes don't display line numbers
; e.g. turn off for pdf as pdf-tools doesn't play well with it
(require 'display-line-numbers)

(defcustom display-line-numbers-exempt-modes
  '(vterm-mode eshell-mode shell-mode term-mode ansi-term-mode pdf-view-mode)
  "Major modes on which to disable line numbers."
  :group 'display-line-numbers
  :type 'list
  :version "green")
(defun display-line-numbers--turn-on ()
  "Turn on line numbers except for certain major modes.
Exempt major modes are defined in `display-line-numbers-exempt-modes'."
  (unless (or (minibufferp)
              (member major-mode display-line-numbers-exempt-modes))
    (display-line-numbers-mode)))



;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.




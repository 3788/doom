;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/packages.el

;; To install a package with Doom you must declare them here and run 'doom sync'
;; on the command line, then restart Emacs for the changes to take effect -- or
;; use 'M-x doom/reload'.


;; To install SOME-PACKAGE from MELPA, ELPA or emacsmirror:
;(package! some-package)

(package! evil-tutor)
(package! which-key)
(package! all-the-icons) ; needed by all-the-icons-dired
(package! all-the-icons-dired)
(package! tablist) ; needed by pdf-tools apparently
(package! pdf-tools)
; https://youtu.be/VcgjTEa0kU4?t=2320
(package! visual-fill-column)

;; GITHUB Packages (i.e. not in MELPA/ELPA)
;; To install a package from github use the following strucure:
; (package! nano-modeline
;   :recipe (:host github :repo "rougier/nano-modeline"))

; Nano look & feel
; Note need to comment out the doom line from init.el if using nano
; Also comment out theme in theme.el
; (package! nano-emacs
;           :recipe (:host github :repo "rougier/nano-emacs"))


;; ORG Packages
(package! org-noter)
; org-bars and org-bullets not compatible. Only load 1
(package! org-bullets)
; (package! org-bars
;           :recipe (:host github :repo "tonyaldon/org-bars"))

; Package with better detangling support: https://gitlab.com/mtekman/org-tanglesync.el
(package! org-tanglesync) 
(package! org-appear) ; reveal hidden emphasis markers under cursor
; (package! org-superstar) ; Nicer headings + bullets
(package! org-download) ; Package to download images
; Org equation live preview:
; https://github.com/guanyilun/org-elp
(package! org-elp) ;
; Toggle Latex previews off when editing
; https://github.com/io12/org-fragtog
(package! org-fragtog) 


;; Prolog
(package! ediprolog)
(package! ob-prolog)
; (package! ob-prolog
;           :recipe (:host github :repo "ljos/ob-prolog"))

;; Theme Packages
(package! almost-mono-themes)
(package! arjen-grey-theme)
(package! atom-dark-theme)
(package! sexy-monochrome-theme)
(package! gruber-darker-theme)
(package! jazz-theme)
(package! clues-theme)
(package! badwolf-theme)
(package! flatland-theme)
(package! white-sand-theme)
(package! hc-zenburn-theme)
(package! nord-theme)


;; To install a package directly from a remote git repo, you must specify a
;; `:recipe'. You'll find documentation on what `:recipe' accepts here:
;; https://github.com/raxod502/straight.el#the-recipe-format
;(package! another-package
;  :recipe (:host github :repo "username/repo"))

;; If the package you are trying to install does not contain a PACKAGENAME.el
;; file, or is located in a subdirectory of the repo, you'll need to specify
;; `:files' in the `:recipe':
;(package! this-package
;  :recipe (:host github :repo "username/repo"
;           :files ("some-file.el" "src/lisp/*.el")))

;; If you'd like to disable a package included with Doom, you can do so here
;; with the `:disable' property:
;(package! builtin-package :disable t)

;; You can override the recipe of a built in package without having to specify
;; all the properties for `:recipe'. These will inherit the rest of its recipe
;; from Doom or MELPA/ELPA/Emacsmirror:
;(package! builtin-package :recipe (:nonrecursive t))
;(package! builtin-package-2 :recipe (:repo "myfork/package"))

;; Specify a `:branch' to install a package from a particular branch or tag.
;; This is required for some packages whose default branch isn't 'master' (which
;; our package manager can't deal with; see raxod502/straight.el#279)
;(package! builtin-package :recipe (:branch "develop"))

;; Use `:pin' to specify a particular commit to install.
;(package! builtin-package :pin "1a2b3c4d5e")


;; Doom's packages are pinned to a specific commit and updated from release to
;; release. The `unpin!' macro allows you to unpin single packages...
;(unpin! pinned-package)
;; ...or multiple packages
;(unpin! pinned-package another-pinned-package)
;; ...Or *all* packages (NOT RECOMMENDED; will likely break things)
;(unpin! t)

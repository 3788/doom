;; Prolog emacs Customizations & Config file

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;; Prolog Mode ;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Make prolog mode the default for .pl extension
(add-to-list 'auto-mode-alist '("\\.\\(pl\\|pro\\|lgt\\)" . prolog-mode))
; Define ediprolog default prolog type to use - swi or scryer 
(setq ediprolog-system 'swi)
; Give the binary path + name
(setq ediprolog-program "/usr/bin/swipl")

; In Prolog mode do ediprolog's do-what-I-mean function on F9
(defun mp-add-prolog-keys ()
  (local-set-key (kbd "<f9>") 'ediprolog-dwim))
(add-hook 'prolog-mode-hook 'mp-add-prolog-keys)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;; ORG Mode & Prolog ;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Allow prolog to run in org mode 
; https://github.com/ljos/ob-prolog
(org-babel-do-load-languages
 'org-babel-load-languages
 '((prolog . t)))

(setq org-babel-prolog-command "swipl")
; (setq org-babel-prolog-command "gprolog")
; (setq org-babel-prolog-command "scryer-prolog")


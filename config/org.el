;;; $DOOMDIR/org.el -*- lexical-binding: t; -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;; ORG DIRECTORIES ;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/"
      org-default-notes-file "~/org/notes.org")
(setq org-agenda-files '("~/org/"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;; ORG BABEL ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq org-babel-python-command "python3")
; (setq org-babel-mathematica-command "MathKernel")
(org-babel-do-load-languages
 'org-babel-load-languages
 '((julia . t)))
;; Load mathematica from contrib
(org-babel-do-load-languages 
 'org-babel-load-languages
 '((mathematica . t))) 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;; ORG IMAGES  ;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'org-download)
;; Drag-and-drop to `dired`
(add-hook 'dired-mode-hook 'org-download-enable)

(setq-default org-download-image-dir "Images/")
(setq-default org-download-heading-lvl 'nil)
; (setq-default org-download-backend '(wget))

(setq org-startup-with-inline-images t)
(setq org-image-actual-width nil)
; (setq org-image-actual-width '(500))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;; ORG LOOK & FEEL ;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; Hide emphasis markers + ~ = // etc.
(setq org-hide-emphasis-markers t)
;; Unhide emphasis markers on cursor line for easy editing
(use-package org-appear
  :hook (org-mode . org-appear-mode))

; Following tweaks from here:
; https://lucidmanager.org/productivity/ricing-org-mode/

;; Improve org mode looks
(setq org-startup-indented t)
(setq org-pretty-entities t)


; Visual Fill mode. See here:
; https://youtu.be/VcgjTEa0kU4?t=2500
(defun org-mode-visual-fill ()
  (setq visual-fill-column-width 90
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :hook (org-mode . org-mode-visual-fill))

; org-bars
; https://github.com/tonyaldon/org-bars
; (require 'org-bars)
; (add-hook 'org-mode-hook 'org-bars-mode)

; From org-bars suggestion to remove now
; redundant ellipsis when headline is folded
; (defun org-no-ellipsis-in-headlines ()
;   "Remove use of ellipsis in headlines.
; See `buffer-invisibility-spec'."
;   (remove-from-invisibility-spec '(outline . t))
;   (add-to-invisibility-spec 'outline))

; (add-hook 'org-mode-hook 'org-no-ellipsis-in-headlines)


; Change the default heading fold symbol
; (setq org-ellipsis " ▼") ; #1
; (setq org-ellipsis " ⟲") ; #2
; (setq org-ellipsis "↺") ; #2
(setq org-ellipsis " ⌄") ; #2
; (setq org-ellipsis " ↻") ; #2
; (setq org-ellipsis "↴") ; #3
; (setq org-ellipsis "⤵") ; #3
; (setq org-ellipsis "⤷") ; #4
; (setq org-ellipsis "⤵"
; (setq org-ellipsis "⬎")
; (setq org-ellipsis "⋱")
; Other interesting characters are ▼, ↴, ⬎, ⤷, and ⋱. "⤵")
; (with-eval-after-load 'org
; (setq org-ellipsis-face '(:foreground "#909070"))) 

  ;; Shorten some text

(defun my/org-mode-setup ()
  (whitespace-mode -1)

  ;; https://orgmode.org/list/87pn8huuq2.fsf@iki.fi/t/
  (electric-indent-local-mode -1)

  ;; Shorten some text
  (setq prettify-symbols-alist
        (map-merge 'list prettify-symbols-alist
                   `(
                     ("#+name:" . "✎")
                     ("#+NAME:" . "✎")
                     ; ("#+HEADER:" . "◎") 
                     ("#+HEADER:" . "▤") 
                     ; ("#+HEADER:" . "H") 
                     ("#+BEGIN_SRC" . "◣")
                     ("#+begin_src" . "◣")
                     ("#+BEGIN_EXAMPLE" . "➤")
                     ("#+END_SRC" . "◤")
                     ; ("#+end_src" . "▰")
                     ("#+end_src" . "◤")
                     ("#+END_EXAMPLE" . "⏹")
                     ; ("#+RESULTS:" . "⇅")
                     ("#+RESULTS:" . "⇣")
                     )))
  (prettify-symbols-mode 0)
  (prettify-symbols-mode)

  ;; Auto-wrap lines
  (visual-line-mode)
  (setq adaptive-wrap-extra-indent 2)

  (variable-pitch-mode)
  ;; from https://lepisma.xyz/2017/10/28/ricing-org-mode/
  ;; A little bit of spacing between lines:
  (setq line-spacing 0.0) ;; doesn't look good on tables.
  ;; A little bit of space in the left/right margins:
  (setq left-margin-width 2)
  (setq right-margin-width 2)
  (set-window-buffer nil (current-buffer))

  (flyspell-mode 1)
  (ws-butler-mode 1))

(add-hook 'org-mode-hook 'my/org-mode-setup)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;; ORG Bullets ;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Org Bullets
; (require 'org-bullets)
; (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

;; Nice bullets
; (use-package org-superstar
;     :config
;     (setq org-superstar-special-todo-items t)
;     (add-hook 'org-mode-hook (lambda ()
;                         (org-superstar-mode 1))))


(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;; STRUCTURE TEMPLATES ;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(with-eval-after-load 'org
  ;; This is needed as of Org 9.2
  (require 'org-tempo)

  (add-to-list 'org-structure-template-alist '("sh" . "src shell"))
  (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
  (add-to-list 'org-structure-template-alist '("cf" . "src conf"))
  (add-to-list 'org-structure-template-alist '("py" . "src python")))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;; ORG ROAM ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; https://systemcrafters.cc/build-a-second-brain-in-emacs/getting-started-with-org-roam/



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;; org-tangleSync  ;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package org-tanglesync
  ; :hook ((org-mode . org-tanglesync-mode)
         ;; enable watch-mode globally:
         ; ((prog-mode text-mode) . org-tanglesync-watch-mode))
  :custom
  (org-tanglesync-watch-files '("openwrt-detangle.org" ))
  :bind
  (( "C-c M-i" . org-tanglesync-process-buffer-interactive)
   ( "C-c M-a" . org-tanglesync-process-buffer-automatic)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;; Org-Noter ;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; OrgNoter Settings
; (setq org-noter-set-start-location "~/org/orgnoter")
; The split fraction is of the form (Horizontal split . Vertical Split)
(setq org-noter-doc-split-fraction '(0.7 . 0.5))
(setq org-noter-notes-search-path '("~/org/orgnoter"))
(setq org-noter-default-notes-file-names '("Notes.org"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;; ORG Latex ;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Highlight latex code in org mode
; (setq org-highlight-latex-and-related '(latex script entities)) ; this is whats usually recommended
(setq org-highlight-latex-and-related '(native)) ; this version works / lookt better than above
; Use the lstlistings package to format code
; (setq org-latex-listings t) 

; Org Equation Live Preview
; https://github.com/guanyilun/org-elp
(use-package org-elp
  :config
  (setq org-elp-idle-time 0.5
        org-elp-split-fraction 0.25))
; Org latex preview options - e.g. colours & scale
(setq org-format-latex-options (plist-put org-format-latex-options :scale 1.6))

; Auto-toggle latex previews off when editing
; https://github.com/io12/org-fragtog
(add-hook 'org-mode-hook 'org-fragtog-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;; ORG Key Mappings  ;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Auto-open a pdf on export from org by pressing f5
(defun org-export-as-pdf-and-open ()
  (interactive)
  (save-buffer)
  (delete-other-windows)
  (split-window-right)
  (windmove-right) 
  (org-open-file (org-latex-export-to-pdf))
  (pdf-view-fit-width-to-window)
  (pdf-view-enlarge  1.2))

(defun org-export-as-pdf ()
  (interactive)
  (save-buffer) 
  (org-latex-export-to-pdf))

(add-hook 
 'org-mode-hook
 (lambda()
   (define-key org-mode-map 
       ; (kbd "<f5>") 'org-export-as-pdf)))
       (kbd "<f5>") 'org-export-as-pdf-and-open)))

(add-hook 
 'org-mode-hook
 (lambda()
   (define-key org-mode-map 
       (kbd "<f6>") 'org-latex-preview)))


; Assign pretty entities toggle to F7 whilst in Org Mode
(add-hook 
  'org-mode-hook
  (lambda()
    (define-key org-mode-map 
                (kbd "<f7>") 'org-toggle-pretty-entities)))

; Assign org-bars-mode toggle to F7
; (add-hook 
;  'org-mode-hook
;  (lambda()
;    (define-key org-mode-map 
;        (kbd "<f7>") 'org-bars-mode)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;; Strike-through DONE TASKS ;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Strikethough done tasks in org 
(defun modify-org-done-face ()
(setq org-fontify-done-headline t)
(set-face-attribute 'org-done nil :strike-through t)
(set-face-attribute 'org-headline-done nil :strike-through t))
(eval-after-load "org"
(add-hook 'org-add-hook 'modify-org-done-face))

; Strike through Done when exporting to Latex
; Source: https://www.javaer101.com/en/article/185021125.html
(defun my-latex-filter-headline-done (text backend info)
  "Ensure dots in headlines."
  (when (org-export-derived-backend-p backend 'latex)
    (save-match-data
      (when (let ((case-fold-search t))
              (string-match "\\\\\\([a-z]+\\){\\(.*DONE.*\\)}"
                            text))
        (if (not (string-match ".*hsout.*" text))
            (replace-match "\\\\\\1{\\\\hsout{\\2}}"
                       t nil text))))))
(eval-after-load 'ox
  '(progn
     (add-to-list 'org-export-filter-headline-functions
                  'my-latex-filter-headline-done)))


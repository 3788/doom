;; My Custom Doom Theme File
;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:


(setq custom-safe-themes t) ;; Don't ask if theme is safe

(setq doom-theme 'doom-monokai-machine) ;really crip theme
; (setq doom-theme 'doom-vibrant)
; (setq doom-theme 'doom-one)
; (setq doom-theme 'doom-palenight)
; (setq doom-theme 'doom-material)
; (setq doom-theme 'doom-nord)
; (setq doom-theme 'doom-nord-light)
; (setq doom-theme 'doom-tomorrow-day)
; (setq doom-theme 'doom-tomorrow-night)
; (setq doom-theme 'doom-spacegrey)
; (setq doom-theme 'doom-solarized-dark)
; (setq doom-theme 'doom-solarized-light)
; (setq doom-theme 'doom-snazzy)
; (setq doom-theme 'doom-peacock)
; (setq doom-theme 'doom-opera-light)
; (setq doom-theme 'doom-opera)
; (setq doom-theme 'doom-molokai)
; (setq doom-theme 'doom-laserwave)
; (setq doom-theme 'doom-Iosvkem)
; (setq doom-theme 'doom-horizon)
; (setq doom-theme 'doom-dark+) ;A *really* good dark-theme with easily visible comments
; (setq doom-theme 'doom-dracula)
; (setq doom-theme 'doom-challener-deep)
; (setq doom-theme 'doom-city-lights)
; (setq doom-theme 'doom-acario-light) ; Another Pretty good theme
; (setq doom-theme 'doom-acario-dark) 
; (setq doom-theme 'wombat)
; (setq doom-theme 'doom-one-light)
; (setq doom-theme 'doom-sourcerer)
; (setq doom-theme 'doom-one)
; (setq doom-theme 'doom-gruvbox)
; (setq doom-theme 'doom-fairy-floss)
; (setq doom-theme 'doom-oceanic-next)
; (setq doom-theme 'doom-moonlight) ; This one is quite good¬
; (setq doom-theme 'doom-nova)
; (setq doom-theme 'doom-wilmersdorf)

; (setq doom-theme 'zenburn)
; (setq doom-theme 'doom-1337)
; (setq doom-theme 'doom-ayu-mirage)

; (setq doom-theme 'doom-monokai-octagon) ;another *really* nice theme
; (setq doom-theme 'doom-plain) ;looks classy, but background too light
; (setq doom-theme 'arjen-grey) ;looks classy, but highlight line too bright
; (setq doom-theme 'atom-dark) ;looks good
; (setq doom-theme 'almost-mono-cream) ;looks good


